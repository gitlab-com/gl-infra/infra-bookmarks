local metricsConfig = import 'gitlab-metrics-config.libsonnet';
local serviceCatalog = import 'service-catalog/service-catalog.libsonnet';
local services = (import 'gitlab-metrics-config.libsonnet').monitoredServices;
local projectsData = std.parseYaml(importstr 'projects.yml');
local saturationResources = import 'servicemetrics/saturation-resources.libsonnet';

local getDescriptiveNameForService(service, serviceCatalogItem) =
  service.type + ' Service - ' + serviceCatalogItem.friendly_name;

local loggingBookmarksForService(service, serviceCatalogItem) =
  local descriptiveName = getDescriptiveNameForService(service, serviceCatalogItem);
  if std.objectHas(serviceCatalogItem, 'technical') && std.objectHas(serviceCatalogItem.technical, 'logging') then
    [{
      label: 'Logging',
      description: 'Logging links for %s' + [descriptiveName],
      bookmarks: [
        {
          label: item.name,
          description: '%s logging for %s' % [item.name, descriptiveName],
          href: item.permalink,
        }
        for item in serviceCatalogItem.technical.logging
      ],
    }]
  else
    [];

local projectBookmarksForService(service, serviceCatalogItem) =
  local descriptiveName = getDescriptiveNameForService(service, serviceCatalogItem);

  local documentation =
    if std.objectHas(serviceCatalogItem, 'technical') && std.objectHas(serviceCatalogItem.technical, 'documents') then
      local documents = serviceCatalogItem.technical.documents;
      (
        if std.objectHas(documents, 'design') then
          [{
            label: 'Design Documentation',
            description: 'Design documedocumentationnts for %s' % [descriptiveName],
            href: documents.design,
          }]
        else []
      )
      +
      (
        if std.objectHas(documents, 'architecture') then
          [{
            label: 'Architecture Documentation',
            description: 'Architecture documentation for %s' % [descriptiveName],
            href: documents.architecture,
          }]
        else []
      )
      +
      (
        if std.objectHas(documents, 'readiness_review') then
          [{
            label: 'Readiness Review Documentation',
            description: 'Readiness review documentation for %s' % [descriptiveName],
            href: documents.readiness_review,
          }]
        else []
      )
      +
      (
        if std.objectHas(documents, 'service') && std.isArray(documents.service) then
          [
            {
              label: url,
              description: 'Service Documentation for %s' % [descriptiveName],
              href: url,
            }
            for url in documents.service
          ]
        else []
      )
    else
      [];

  if documentation == [] then
    []
  else
    [{
      label: 'Documentation',
      description: 'Documentation to %s' % [descriptiveName],
      bookmarks: documentation,
    }];

local bookmarksForServiceCatalogDocumentation(service, serviceCatalogItem) =
  local descriptiveName = getDescriptiveNameForService(service, serviceCatalogItem);

  if std.objectHas(serviceCatalogItem, 'technical') && std.objectHas(serviceCatalogItem.technical, 'project') && std.isArray(serviceCatalogItem.technical.project) then
    [{
      label: 'Projects',
      description: '',
      bookmarks: [
        {
          label: item,
          description: 'Project link for %s' % [descriptiveName],
          href: item,
        }
        for item in serviceCatalogItem.technical.project
      ],
    }]
  else
    [];

local saturationBookmarksForService(service, serviceCatalogItem) =
  local serviceType = service.type;
  local descriptiveName = getDescriptiveNameForService(service, serviceCatalogItem);

  local saturationPoints = saturationResources.listApplicableServicesFor(serviceType);
  if saturationPoints != [] then
    [{
      label: 'Saturation Dashboards',
      description: 'Saturation dashboards for %s' % [descriptiveName],
      bookmarks: [
        local item = saturationResources[componentName];
        {
          label: "%s (%s)" % [item.title, componentName],
          description: '%s for %s' % [item.title, descriptiveName],
          href: 'https://dashboards.gitlab.net/d/alerts-%s?orgId=1&var-type=%s' % [item.grafana_dashboard_uid, serviceType],
        }
        for componentName in saturationPoints
      ],
    }]
  else
    [];

local bookmarksFolderForService(service) =
  local serviceType = service.type;
  local serviceCatalogItem = serviceCatalog.lookupService(serviceType);

  local descriptiveName = getDescriptiveNameForService(service, serviceCatalogItem);

  {
    label: descriptiveName,
    description: 'Bookmarks for %s' % [descriptiveName],
    folders:
      loggingBookmarksForService(service, serviceCatalogItem)
      +
      projectBookmarksForService(service, serviceCatalogItem)
      +
      saturationBookmarksForService(service, serviceCatalogItem),
    bookmarks:
      [{
        label: '%s Service Overview Grafana Dashboard' % [serviceType],
        description: 'Grafana Service overview dashboard for %s' % [descriptiveName],
        href: 'https://dashboards.gitlab.net/d/%s-main/%s-overview?orgId=1' % [serviceType, serviceType],
      }, {
        label: '%s Service Grafana Folder' % [serviceType],
        description: 'Grafana folder for %s' % [descriptiveName],
        href: 'https://dashboards.gitlab.net/dashboards/f/%s/%s' % [serviceType, serviceType],
      }, {

        label: '%s Runbooks' % [serviceType],
        description: 'Runbooks for %s' % [descriptiveName],
        href: 'https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/%s' % [serviceType],
      }]
      +
      bookmarksForServiceCatalogDocumentation(service, serviceCatalogItem),
  };

local bookmarksForGitLabProjects() =
  local projectKeys = std.objectFields(projectsData);

  std.map(
    function(projectKey)
      local p = projectsData[projectKey];

      {
        label: p.name,
        description: p.description,
        href: p.link,
      },
    projectKeys
  );
{
  'bookmarks.yaml':
    {
      label: 'GitLab Infrastructure',
      description: 'Useful bookmarks for GitLab SREs',
      folders: [
        {
          label: 'Services',
          description: 'Bookmarks for GitLab Services',
          folders: [
            bookmarksFolderForService(service)
            for service in services
          ],
        },
        {
          label: 'GitLab Projects',
          description: 'GitLab Projects',
          bookmarks: bookmarksForGitLabProjects(),
        },
      ],
    },
}
