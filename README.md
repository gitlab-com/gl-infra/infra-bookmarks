# GitLab Infrastructure Bookmarks

![Screenshot of Bookmarks](img/screenshot.png)

Bookmarks for GitLab Infrastructure SREs.

## How to use this project

1. run `curl https://gitlab-com.gitlab.io/gl-infra/infra-bookmarks/ -o bookmarks.html`
1. In your browser, choose "Import Bookmarks from HTML File", then choose the file `bookmarks.html`
1. Look for a folder named something like `Imported`.
1. When you need to access a infrastructure link quickly, just use the bookmark!

Inspired by <https://blog.devgenius.io/how-i-centralize-and-distribute-my-bookmarks-cc8b11bb7475>.

## Where does the data come from?

This project relies on metadata stored in the [Runbooks](https://gitlab.com/gitlab-com/runbooks) project, in the service-catalog, and the metrics-catalog.

