#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

REPO_DIR=$(
  cd "$(dirname "${BASH_SOURCE[0]}")/.."
  pwd
)

main() {
  clone_runbooks
  pull_handbook_data
  render_multi_jsonnet "${REPO_DIR}" "${REPO_DIR}/bookmarks.jsonnet"
  npm run generate
  mv "${REPO_DIR}/public/browsers.html" "${REPO_DIR}/public/index.html"
}

clone_runbooks() {
  if [ ! -d runbooks ]; then
    git clone "https://gitlab.com/gitlab-com/runbooks.git" runbooks
    cd "runbooks"
  else
    cd "runbooks"
    git clean -fd
    git checkout master
    git pull
  fi

  jb install
  cd -
}

pull_handbook_data() {
  curl --silent https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/projects.yml -o projects.yml
}


function render_multi_jsonnet() {
  local dest_dir="$1"
  local filename="$2"

  jsonnet-tool render \
    --multi "$dest_dir" \
    -J "${REPO_DIR}/runbooks/libsonnet/" \
    -J "${REPO_DIR}/runbooks/metrics-catalog/" \
    -J "${REPO_DIR}/runbooks/services/" \
    -J "${REPO_DIR}/runbooks/vendor/" \
    "${filename}"
}

main
